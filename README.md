# DS103 Designing for Beginners



## Introduction

On this site, I collect and share materials related to the course DS103 Designing for Beginners, which I teach in the fall semester of 2023 at the [School of Design](http://designschool.sustech.edu.cn) at [Southern University of Science and Technology](https://www.sustech.edu.cn) in Shenzhen. Materials accumulate here as the semester progresses, week by week.

DS103 offers a foundational introduction to design through action and a preparation for design studies at the [School of Design](http://designschool.sustech.edu.cn). It establishes design as an activity in a range of contexts, moving beyond problem solving to opportunity seeking. The content examines how design is translated to outcomes to contribute to economies and quality of life. As a preparation for design studies, the course presents an outline of the study experience and an overview of the curriculum and focus areas offered at the [School of Design](http://designschool.sustech.edu.cn).

With its focus on design as an activity, DS103 is structured around 16 verbs that are central to the practice of design:

![DS103 Overview](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/preview-images/DS103_overview.jpg)


## Week 01: To Transform

[15-Sep-2023 Slide Deck: To Transform](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_01_Welcome_Transform.pdf)

![DS103 Week 01 Slide 35](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/preview-images/DS103_WK01_preview01.jpg)

Key take-away (Slide 35): _When there is friction between our internal and external environments, we tend to adjust our environment to meet our inner needs. However, we increasingly depend on our readiness to adjust our inner selves to environmental needs instead._


## Week 02: Associative Geometry; To Observe

![DS103 Week 02 Meeting 1 Rhino/Grasshopper Demo](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK02_preview01.jpg)

[22-Sep-2023 Meeting 2 Slide Deck: To Observe](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_02_Observe.pdf)

![DS103 Week 02 Meeting 2 Slide 23](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK02_preview02.jpg)

Key take-away (Slide 23): _There are several modes of (user) observation in (design) research such as naturalistic/controlled, direct/indirect. Other modes include overt and covert observation and different degrees of researcher participation._


## Week 03: To Muddle Through

[08-Oct-2023 Slide Deck](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_02B_Muddle_Through.pdf)

![DS103 Week 03 Slide 24](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK03_preview01.jpg)

Key take-away (Slide 24): _Muddling through the design process involves dealing with incomplete information, satisficing and "throwing things at the wall and seeing what sticks."_


## Week 04: Tools, Material, and Language; To Question

[11-Oct-2023 Slide Deck: Tools, Material, and Language](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_02C_Tools_Material_Language.pdf)

![DS103 Week 04 Meeting 1 Slide 22](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK04_preview01.jpg)

Key take-away (Slide 22): _Code relates to data as tools relate to materials, and some minds are less constrained by these categories than others._

[13-Oct-2023 Slide Deck: To Question](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_03_Question.pdf)

![DS103 Week 04 Meeting 2 Slide 28](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK04_preview02.jpg)

Key take-away (Slide 28): _We both hold on to what we know and venture into the unknown. Systematic doubt and questioing are means to find the new._


## Week 05: To Explore

[20-Oct-2023 Slide Deck: To Explore](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_04_Explore.pdf)

![DS103 Week 05 Meeting 2 Slide 03](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK05_preview01.jpg)

Key take-away (Slide 03): _In most cases, good design is not ready to be picked up without effort. Design exploration - as exploration for natural resources - often requires the probing and analysing a great deal of candidate "material" so that valuable items may be found._


## Week 06: Sketching Basics 1, Parametric Optimization, To Empathize

[25-Oct-2023 Slide Deck: Sketching Basics 1 by Boyu Bao](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_05A_Sketching_Basics_01.pdf)

![DS103 Week 06 Meeting 1A Slide 05](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK06_preview01.jpg)

[25-Oct-2023 Slide Deck: Parametric Optimization](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_05B_Parametric_Optimization.pdf)

![DS103 Week 06 Meeting 1B Slide 05](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK06_preview02.jpg)

Key take-away (Slide 05): _Optimization solvers approach the relationship between inputs and outputs as a black box. They automatically vary the inputs, observe the outputs, and seek to drive the output towards specified objectives such as minima, maxima or approximations of user-specified values."_

[27-Oct-2023 Slide Deck: To Empathize Part 1](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_05_Empathize_Part_1.pdf)

![DS103 Week 06 Meeting 1C Slide 11](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK06_preview03.jpg)

Key take-away (Slide 11): _Individuals may differ in terms of their willingness and readiness to empathize. Explicit requests to "put oneself into the shows of the other" may help individuals appreciate the "golden rule" and develop empathetic attitudes._


## Week 07: To Research

[03-Nov-2023 Slide Deck: To Empathize Part 2, To Research](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_06_Empathize_Pt_2_Research.pdf)

![DS103 Week 07 Slide 25](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK07_preview01.jpg)

Key take-away (Slide 25): _Christopher Freyling distinguishes three kinds of design research: Research through design, research for design, and research about design._


## Week 08: Sketching Basics 2, Correlation, To Risk

[08-Nov-2023 Slide Deck: Sketching Basics 2 by Boyu Bao](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_07A_Sketching_Basics_02.pdf)

![DS103 Week 08 Meeting 1A Slide 07](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK08_preview01.jpg)

[08-Nov-2023 Slide Deck: Correlations](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_07B_Research_Correlation.pdf)

![DS103 Week 08 Meeting 1B Slide 06](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK08_preview02.jpg)

Key take-away (Slide 06): _Correlation dies not imply causation. Two co-occurring observations may be caused by a third element, or they may correlate spuriously."_

[10-Nov-2023 Slide Deck: To Risk](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_07C_Risk.pdf)

![DS103 Week 08 Meeting 1C Slide 15](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK08_preview03.jpg)

Key take-away (Slide 15): _Design is a lossy process. It demands extensive investment of time, effort, materials, tools and so on. Returns on such investment is often difficult to predict."_


## Week 09: To Converse, To Make

[17-Nov-2023 Slide Deck: To Converse, To Make](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_08_09_Converse_Make.pdf)

![DS103 Week 09 Slide 23](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK09_preview01.jpg)

Key take-away (Slide 23): _Various fields (such as engineering) are based on a strict understanding of control. These fields avoid error, noise, and misunderstanding. Creative design exploration, by contrast, aims to get out-of-control, seeking error, noise and misunderstanding as sources of novelty._

![DS103 Week 09 Slide 40](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK09_preview02.jpg)

Key take-away (Slide 40): _Designers make things for a broad spectrum of purposes, ranging from epistemic objects that help us to find (new) things (out) to technical objects that represent what we wish others to understand or appreciate._


## Week 10: Sketching Basics 3: Reverse Thinking

[22-Nov-2023 Meeting 1 Slide Deck: Sketching Basics 3 - Reverse Thinking by Boyu Bao](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_Sketching_Basics_03_Reverse_Thinking.pdf)

![DS103 Week 10 Meeting 2 Slide 05](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK10_preview01.jpg)


## Week 11: To Simplify

[01-Dec-2023 Slide Deck: To Simplify](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_10_Simplify.pdf)

![DS103 Week 11 Slide 25](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK11_preview01.jpg)

Key take-away (Slide 25): _Simplicity lies in the eye of the beholder. For example, an object A may be simpler than object B in terms of its shape, while object B is simpler than object A in terms of contained technological systems and components._


## Week 12: To Re-Combine

[08-Dec-2023 Slide Deck: To Re-Combine](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_11_Recombine.pdf)

![DS103 Week 12 Slide 28](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK12_preview01.jpg)

Key take-away (Slide 28): _The recombining of elements and modular components can serve a variety of purposes and benefits across the design process, production and use stages._


## Week 13: To Surprise

[15-Dec-2023 Slide Deck: To Re-Surprise](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_12_Surprise.pdf)

![DS103 Week 13 Slide 08](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK13_preview01.jpg)

Key take-away (Slide 08): _An element of surprise can elevate a design beyond the banal._


## Week 14: To Re-Appropriate, To Model and To Code

[20-Dec-2023 Slide Deck: To Re-Surprise](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_13_Reappropriate.pdf)

![DS103 Week 14 Meeting 1 Slide 43](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK14_preview01.jpg)

Key take-away (Slide 43): _Designers can turn anything into anything._


[22-Dec-2023 Slide Deck: To Model, To Code](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_14_Code.pdf)

![DS103 Week 14 Meeting 2 Slide 03](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK14_preview02.jpg)

Key take-away (Slide 03): _We use models for a broad range of purposes, typically where using the modeled entities themselves would be too expensive, too time-consuming, too dangerous, end so on._


## Week 15: To Propose

[29-Dec-2023 Slide Deck: To Propose](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_15_Propose.pdf)

![DS103 Week 15 Slide 20](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK15_preview01.jpg)

Key take-away (Slide 20): _._


## Week 16: Summary, Music

[03-Jan-2023 Meeting 1 Slide Deck: Summary](https://gitlab.com/tomfischer/DS103-Designing-for-Beginners/-/raw/main/slide-decks/DS103_Designing_for_Beginners_16_Summary.pdf)

![DS103 Week 16 Meeting 1 Slide 02](https://gitlab.com/tomfischer/DS103/-/raw/main/preview-images/DS103_WK16_preview01.jpg)

Key take-away (Slide 02): _._
